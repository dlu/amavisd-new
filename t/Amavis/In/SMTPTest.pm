# SPDX-License-Identifier: GPL-2.0-or-later

package Amavis::In::SMTPTest;

use Test::Most;
use base 'Test::Class';

use Cwd;
use File::Temp;

sub class { 'Amavis::In::SMTP' }

sub startup : Tests(startup => 1) {
  my $test = shift;
  use_ok $test->class;
  chdir('/');
  $Amavis::Conf::TEMPBASE = File::Temp->newdir('test-directory-tmp-XXXXX', CLEANUP => 0, TMPDIR => 1)->dirname;
}

sub after : Test(teardown => 1) {
  my $tmpdir = $Amavis::Conf::TEMPBASE;
  $Amavis::Conf::TEMPBASE = undef;
  chdir('/');
  ok rmdir($tmpdir), '... and temp dir should be empty and removable';
}

sub constructor : Tests(3) {
  my $test  = shift;
  my $class = $test->class;
  can_ok $class, 'new';
  ok my $lookup = $class->new,
  '... and the constructor should succeed';
  isa_ok $lookup, $class, '... and the object it returns';
}

1;
