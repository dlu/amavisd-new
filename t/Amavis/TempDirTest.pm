# SPDX-License-Identifier: GPL-2.0-or-later

package Amavis::TempDirTest;

use Test::Most;
use base 'Test::Class';

sub class { 'Amavis::TempDir' }

sub startup : Tests(startup => 1) {
  my $test = shift;
  use_ok $test->class;
}

sub empty : Tests(0) {
}

1;
