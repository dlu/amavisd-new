# SPDX-License-Identifier: GPL-2.0-or-later

package Amavis::Lookup::SQLTest;

use Test::Most;
use base 'Test::Class';

sub class { 'Amavis::Lookup::SQL' }

sub startup : Tests(startup => 1) {
  my $test = shift;
  use_ok $test->class;
}

sub constructor : Tests(3) {
  my $test  = shift;
  my $class = $test->class;
  can_ok $class, 'new';
  ok my $lookup = $class->new(0,'testclause'),
  '... and the constructor should succeed';
  isa_ok $lookup, $class, '... and the object it returns';
}

1;
